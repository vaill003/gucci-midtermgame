﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gucci : MonoBehaviour {

	public float jumpForce = 200f; // la valeur du saut
	private bool isded = false;
	private Rigidbody2D rb2d;
	private Animator anim;

	// Use this for initialization
	void Start () {
		rb2d = GetComponent<Rigidbody2D>();
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if (isded == false) // si le joueur est en vie : début & jeu
		{
			if (Input.GetMouseButtonDown(0))
			{
				rb2d.velocity = Vector2.zero;
				rb2d.AddForce(new Vector2(0, jumpForce));
				anim.SetTrigger("Flap");
			}
		}
	}

	void OnCollisionEnter2D()
	{
		rb2d.velocity = Vector2.zero;
		isded = true;
		anim.SetTrigger("Ded");
		GameControl.instance.GucciDied();
	}
}
